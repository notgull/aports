# Contributor: fossdd <fossdd@pwned.life>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=pipx
pkgver=1.6.0
pkgrel=0
pkgdesc="Install and Run Python Applications in Isolated Environments"
url="https://github.com/pypa/pipx"
arch="noarch"
license="MIT"
depends="
	py3-argcomplete
	py3-colorama
	py3-packaging
	py3-platformdirs
	py3-userpath
	"
makedepends="
	py3-gpep517
	py3-hatchling
	py3-hatch-vcs
	"
checkdepends="
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/pypa/pipx/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # need some packages that i'm not sure of

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -p no:warnings
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
a91cf61b0cf8d51aadb7c9f2a22de70b07e3dd030553d0706ff4e924100ee0aa27dda5450ad6ad2de9b92eab7958c3ed26adbcdcf2217aa1a626f943175e1262  pipx-1.6.0.tar.gz
"
